from django.urls import path,include
from .views import *

app_name = 'crm'
urlpatterns = [
	path('',main,name='main'),
	path('export/',export,name='export'),
	path('post-card/<int:in_id>',post_card,name='post_card'),
	path('upload/',upload,name='upload'),
	path('contact/', ContactList.as_view(),name='contact'),
	path('contact-indv/<int:c_id>', indivisual_contact,name='indivisual_contact'),
	path('contact-udate/<int:pk>', ContactUpdate.as_view(),name='ContactUpdate'),
	path('contact-delete/<int:pk>', ContactDelete.as_view(),name='ContactDelete'),
	path('contact-create/', ContactCreate.as_view(),name='ContactCreate'),
]


