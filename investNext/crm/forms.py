from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from .models import Contact



class UserRegisterForm(UserCreationForm):
    username=forms.CharField(widget=forms.TextInput(attrs={
    	'class':'field',
    	'type':'text',
    	'placeholder':'First name'
    	}))
    email=forms.CharField(widget=forms.TextInput(attrs={
    	'class':'field',
    	'type':'email',
    	'placeholder':'First name'
    	}))
    password1=forms.CharField(widget=forms.TextInput(attrs={
    	'class':'field',
    	'type':'password',
    	'placeholder':'First name'
    	}))
    password2=forms.CharField(widget=forms.TextInput(attrs={
    	'class':'field',
    	'type':'password',
    	'placeholder':'First name'
    	}))
    first_name=forms.CharField(widget=forms.TextInput(attrs={
    	'class':'field',
    	'type':'text',
    	'placeholder':'First name'
    	}))
    last_name=forms.CharField(widget=forms.TextInput(attrs={
    	'class':'field',
    	'type':'text',
    	'placeholder':'First name'
    	}))

class ContactForm(forms.ModelForm):
	class Meta:
		model = Contact
		fields = '__all__'
		exclude = ('user',)
		widget={
		'first_name': forms.TextInput(attrs={
				'class':'field',
				'type':'text',
				'placeholder':'First name'
								}),
		'last_name': forms.TextInput(attrs={
				'class':'field',
				'type':'text',
				'placeholder':'Last name'
								}),
		'email': forms.TextInput(attrs={
				'class':'field',
				'type':'email',
				'placeholder':'@email.com'
								}),
		'address_one': forms.TextInput(attrs={
				'class':'field',
				'type':'text',
				'placeholder':'11124 Cajon park'
								}),
		'address_two': forms.TextInput(attrs={
				'class':'field',
				'type':'text',
				'placeholder':'Ape,Bld,Blvd'
								}),
		'city': forms.TextInput(attrs={
				'class':'field',
				'type':'text',
				'placeholder':'San Diego'
								}),
		'state': forms.TextInput(attrs={
				'class':'field',
				'type':'text',
				'placeholder':'CA'
								}),
		'code': forms.TextInput(attrs={
				'class':'field',
				'type':'text',
				'placeholder':'92020'
								}),
		'phone_number': forms.TextInput(attrs={
				'class':'field',
				'type':'text',
				'placeholder':'1234567890'
								}),
		}




