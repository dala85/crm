from django.db import models
from django.urls import reverse
from django.contrib.auth.models import User

class Contact(models.Model):

	user = models.ForeignKey(User,on_delete=models.CASCADE)
	first_name = models.CharField(max_length=100)
	last_name = models.CharField(max_length=100)
	email = models.EmailField(max_length=70, blank=True)
	address_one = models.CharField(max_length=100, blank=True)
	address_two = models.CharField(max_length=100, blank=True)
	city = models.CharField(max_length=40, blank=True)
	state = models.CharField(max_length=20, blank=True)
	code = models.CharField(max_length=6,blank=True)
	phone_number = models.CharField(max_length=12,blank=True)

	def __str__(self):
		return f'{self.user} - {self.first_name}'

	def get_absolute_url(self):
		return reverse('crm:indivisual_contact',kwargs={'pk':self.id})