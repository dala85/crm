from django.shortcuts import render,redirect,get_object_or_404
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import ListView,CreateView,UpdateView,DeleteView
from django.contrib.auth.decorators import login_required
from .forms import UserRegisterForm,ContactForm
from .models import Contact
from django.urls import reverse
from django.views.generic.edit import FormMixin
from django.http import HttpResponse
from .resources import ContactResource
from tablib import Dataset
from django.contrib.auth.models import User
import io
import csv
import lob
lob_key = '026e7634-24d7-486c-a0bb-4a17fd0eebc5'


""" Landing page """
def main(request):
    return render(request,'contacts/main.html',{})

""" Register new user """
def register(request):
    if request.method == 'POST':
        form = UserRegisterForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            return redirect('login')
    else:
        form = UserRegisterForm()
    return render(request, 'users/register.html', {'form': form})



""" All contacts for an indiviusal which is the authenticated user"""
class ContactList(LoginRequiredMixin,ListView):
    template_name   =   'contacts/contact_list.html'
    queryset        =   Contact.objects.all()
    def get_queryset(self):
        return Contact.objects.filter(user=self.request.user)


""" One contacts from the user list"""
def indivisual_contact(request,c_id):
    contact = get_object_or_404(Contact,pk=c_id)
    return render(request,'contacts/indivisual_contact.html',{'contact':contact})


"""Update the particular contact"""
class ContactUpdate(LoginRequiredMixin,UpdateView):
    template_name   =  'contacts/contact_update.html'
    form_class      =   ContactForm
    model           =   Contact

    def get_success_url(self):
        return reverse('crm:contact')

""" Delete the particular contact """
class ContactDelete(LoginRequiredMixin,DeleteView):
    template_name   =   'contacts/contacts_delete.html'
    model           =   Contact


    def get_success_url(self):
        return reverse('crm:contact')
  
""" Create new contact """
class ContactCreate(LoginRequiredMixin,CreateView):
    template_name   =   'contacts/contact_create.html'
    form_class      =   ContactForm
    queryset        =   Contact.objects.all()

    def form_valid(self, form):
        self.object         = form.save(commit=False)
        self.object.user    = self.request.user
        self.object.save()
        return FormMixin.form_valid(self, form) 
        
    def get_success_url(self):
        return reverse('crm:contact')

"""
    Export Excel file for the indivisula list
    response = HttpResponse(dataset.csv, content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="persons.csv"'
"""
def export(request, **kwargs):
    queryset = Contact.objects.filter(user=request.user)
    person_resource = ContactResource()
    dataset = person_resource.export(queryset)
    response = HttpResponse(dataset.xls, content_type='application/vnd.ms-excel')
    response['Content-Disposition'] = 'attachment; filename="persons.xls"'
    
    return response



"""
Upload contact from CSV file, the columns must matches, 
future works will display the list to the user in order 
to make some changes and then submit it
"""
def upload(request):
    try:
        if request.method == 'POST':
            file = request.FILES['myfile']
            data_set = file.read().decode('utf-8')
            print(data_set)
            data_string = io.StringIO(data_set)
            next(data_string)
            for column in csv.reader(data_string,delimiter=',',quotechar='|'):
                created = Contact.objects.update_or_create(
                    user=request.user,
                    first_name = column[0],
                    last_name = column[1],
                    email = column[2],
                    address_one = column[3],
                    address_two = column[4],
                    city = column[5],
                    state = column[6],
                    code = column[7],
                    phone_number = column[8],

                    )

        response = HttpResponse('crm:contact')
        return response
    except:
        return redirect('crm:contact')

"""
Send post card to selected person and the person should have 
his firs, last name and his email in the database
"""
def post_card(request,in_id):
    indivisual = Contact.objects.get(pk=in_id)
    if indivisual.email and indivisual.first_name and indivisual.last_name:
        try:
            lob.Postcard.create(
              to_address = indivisual.email,
              from_address = f'{request.user.first_name} {request.user.last_name}',
              front = 'https://lob.com/postcardfront.pdf',
              back = 'https://lob.com/postcardback.pdf',
              headers = {
                'Idempotency-Key': lob_key,
              }
            )
        except:
            return redirect('crm:contact')
    return render(request,'contacts/post_card.html',{})
